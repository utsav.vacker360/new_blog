from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post, Category, Tag
from . forms import FeedbackForm
from django.contrib import messages
from django.core.paginator import Paginator


# def post_list(request):
#     post = Post.objects.all().order_by('-pub_date')
#     # pagintator = Paginator(post, 3)
#     return render(request, 'blog/index.html',{'posts':post})

##LisView for post list
class PostListView(ListView):
    model = Post
    template_name = 'blog/index.html'
    context_object_name = 'posts'
    ordering = ['-pub_date']

# def post_detail(request, pk, slug):
#     post = get_object_or_404(Post, pk=pk)
#     # post = Post.objects.get(pk=pk)
#     return render(request, 'blog/post_detail.html',{'post':post})

#PostDetailView
class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post'

def post_by_category(request, slug):
    category = get_object_or_404(Category, slug = slug)
    # category = Category.objects.get(slug = slug)
    post = get_list_or_404(Post.objects.order_by('-pub_date'), category__slug = slug)
    # post = Post.objects.filter(category__slug = slug)

    context = {
        'category': category,
        'posts': post
    }

    return render(request, 'blog/post_by_category.html', context)

def post_by_tag(request, slug):
    tag = get_object_or_404(Tag, slug = slug)
    # tag = Tag.objects.get(slug = slug)
    post = get_list_or_404(Post.objects.order_by('-pub_date'), tags__slug = slug)
    # post = Post.objects.filter(tags__slug = slug)

    context = {
        'tag':tag,
        'posts': post
    }

    return render(request, 'blog/post_by_tag.html', context)

def test_redirect(request):
    return HttpResponseRedirect('https://www.google.com')

def feedback(request):
    if request.method == 'POST':
        f = FeedbackForm(request.POST)
        if f.is_valid():
            f.save()
            message = messages.add_message(request, messages.INFO, 'Feedback Submitted')
            
            return redirect('blog:feedback')

    else:
        f = FeedbackForm()
    return render(request, 'blog/feedback.html', {'form':f})
