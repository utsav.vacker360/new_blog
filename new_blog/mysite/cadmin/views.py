from django.shortcuts import render, redirect, get_object_or_404, reverse, Http404
from blog.forms import PostForm
from blog.models import Post, Author, Category, Tag
from django.contrib import messages
from django.conf import settings
from blog.urls import urlpatterns
from . forms import UserRegisterForm, ProfileUpdatForm, UserUpdateForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.mail import send_mail
from mysite import helpers
from django.views.generic import CreateView, UpdateView, DeleteView
# Create your views here.
def register(request):
    if request.method == 'POST':

        form = UserRegisterForm(request.POST)
        if form.is_valid():
            activation_key = helpers.generate_activation_key(username = request.POST['username'])
            subject = 'Django Blog Account Activation'
            message = f'''\n
            please cluck on the link below to activate your account: \n
            {request.scheme}://{request.get_host()}/cadmin/activate/account/?key={activation_key}
            '''
            error = False
            try:

                send_mail(subject, message, settings.SERVER_EMAIL, [request.POST['email']])
                messages.success(request,f'Account created! Click on the link sent to your email to activate the account')
            except:
                error = True
                messages.success(request,'Unable to send email. Please try again')
            
            if not error:
                u = User.objects.create_user(request.POST['username'],
                                            request.POST['email'],
                                            request.POST['password1'],
                                            is_active = 0)
                author = Author()
                author.activation_key = activation_key
                author.user = u
                author.save()

            
            return redirect('cadmin:register')
    else:
        form = UserRegisterForm()
    return render(request, 'cadmin/register.html', {'form':form})


def activate_account(request):
    key = request.GET['key']
    if not key:
        raise Http404()

    r = get_object_or_404(Author, activation_key=key, email_validated=False)
    r.user.is_active = True
    r.user.save()
    r.email_validated = True
    r.save()

    return render(request, 'cadmin/activated.html')

    
@login_required
def profile(request):
    if request.method == 'POST':
        
        u_form = UserUpdateForm(request.POST,instance=request.user)
        p_form = ProfileUpdatForm(request.POST,
                                    request.FILES,
                                    instance=request.user.author)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, 'Profile Updated!')
            return redirect(reverse('cadmin:profile'))

    else:
        if request.user.is_staff:
            return render(request, 'cadmin/profile.html')
        else:

            u_form = UserUpdateForm(instance=request.user)
            p_form = ProfileUpdatForm(instance=request.user.author)
        

            context = {
                        'u_form':u_form,
                        'p_form':p_form
                        }
            return render(request, 'cadmin/profile.html',context)



# @login_required
# def post_add(request):
#     if request.method == 'POST':
#         # creating a bound form with form data
#         f = PostForm(request.POST) 

#         if f.is_valid():
#             f.save()
#             messages.add_message(request, messages.SUCCESS,'Post Added!')
#             return redirect('cadmin:post_add')
#     else:
#         # show unbound form to user
#         f = PostForm()

#         return render(request, 'cadmin/post_add.html',{'form':f})

#Post CreateView
class PostCreateView(LoginRequiredMixin, CreateView):

    model = Post
    template_name = 'cadmin/post_form.html'
    fields = ['title', 'content','category','tags']
    def form_valid(self,form):
        form.instance.author = Author.objects.get(user = self.request.user)
        return super().form_valid(form)



# @login_required
# def post_update(request, pk):
#     post = get_object_or_404(Post, pk = pk)

#     if request.method == 'POST':
#         f = PostForm(request.POST, instance=post)
#         if f.is_valid():
#             f.save()
#             messages.add_message(request, messages.INFO,'Post Updated!')
#             return redirect(reverse('cadmin:post_update', args=[post.pk]))
#     else:
#         f = PostForm(instance=post)
#         return render(request,'cadmin/post_update.html', {'form':f,'post':post})

#PostUpdateView
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):

    model = Post
    template_name = 'cadmin/post_form.html'
    fields = ['title', 'content','category','tags']
    def form_valid(self,form):
        form.instance.author = Author.objects.get(user = self.request.user)
        return super().form_valid(form)

    def test_func(self):
        post  = self.get_object()
        a = Author.objects.get(user = self.request.user)
        
        if a == post.author:

            return True
        return False


#PostDetailView


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin,DeleteView):
    model = Post
    success_url = '/'
    template_name = 'cadmin/post_confirm_delete.html'
    def test_func(self):
        post  = self.get_object()
        a = Author.objects.get(user = self.request.user)
        
        if a == post.author:
            return True
        return False