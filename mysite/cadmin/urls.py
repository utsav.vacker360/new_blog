from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from .views import PostCreateView, PostUpdateView, PostDeleteView


app_name = 'cadmin'

urlpatterns = [
    path('activate/account/', views.activate_account, name = 'activate'),
    path('register/', views.register, name = 'register'),
    path('login/', auth_views.LoginView.as_view(template_name = 'cadmin/login.html'), name = 'login'),
    path('logout/', auth_views.LogoutView.as_view(template_name = 'cadmin/logout.html'), name = 'logout'),
    path('profile/',views.profile,name= 'profile'),
    # path('post/add/',views.post_add,name= 'post_add'),
    path('post/add/', PostCreateView.as_view(),name = 'post-create'),
    # path('post/update/<int:pk>/', views.post_update, name = 'post_update'),
    path('post/update/<int:pk>/', PostUpdateView.as_view(), name = 'post-update'),
    path('post/delete/<int:pk>/', PostDeleteView.as_view(), name = 'post-delete')



]