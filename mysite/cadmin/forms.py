from django import forms
from django.contrib.auth.models import User
from blog.models import Author
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User

        fields = ['username', 'email', 'password1', 'password2']

class UserUpdateForm(forms.ModelForm):
    class Meta:
        email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']

class ProfileUpdatForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['pp']