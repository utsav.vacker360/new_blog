from django.urls import path
from .views import PostListView, PostDetailView
from . import views

app_name = 'blog'

urlpatterns = [
    # path('',views.post_list, name='home'),
    path('',PostListView.as_view(), name='home'),
    # path('<int:pk>/<slug:slug>/', views.post_detail, name='detail'),
    path('<int:pk>/<slug:slug>/', PostDetailView.as_view(), name = 'detail'),
    path('category/<slug:slug>/',views.post_by_category,name='post_by_category'),
    path('tag/<slug:slug>/', views.post_by_tag, name = 'post_by_tag'),
    path('blog/',views.test_redirect, name = 'test_redirect'),
    path('feedback/',views.feedback, name = 'feedback'),

]